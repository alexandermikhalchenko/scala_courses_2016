package calculator

sealed abstract class Expr
final case class Literal(v: Double) extends Expr
final case class Ref(name: String) extends Expr
final case class Plus(a: Expr, b: Expr) extends Expr
final case class Minus(a: Expr, b: Expr) extends Expr
final case class Times(a: Expr, b: Expr) extends Expr
final case class Divide(a: Expr, b: Expr) extends Expr

object Calculator {
  def computeValues(namedExpressions: Map[String, Signal[Expr]]): Map[String, Signal[Double]] = {
    namedExpressions.map(kv =>
      kv._1 -> Signal(eval(getReferenceExpr(kv._1, namedExpressions), namedExpressions))
    )
  }

  def eval(expr: Expr, references: Map[String, Signal[Expr]]): Double = {
    def hlp(expr: Expr, references: Map[String, Signal[Expr]], acc: Set[String]): Double = {
      expr match {
        case Literal(v) => v
        case Ref(name) if acc.contains(name) || references.get(name).isEmpty => Double.NaN
        case Ref(name) => hlp(getReferenceExpr(name, references), references, acc + name)
        case Plus(a, b) => hlp(a, references, acc) + hlp(b, references, acc)
        case Minus(a, b) => hlp(a, references, acc) - hlp(b, references, acc)
        case Times(a, b) => hlp(a, references, acc) * hlp(b, references, acc)
        case Divide(a, b) => hlp(a, references, acc) / hlp(b, references, acc)
      }
    }

    hlp(expr, references, Set.empty)
  }

  /** Get the Expr for a referenced variables.
   *  If the variable is not known, returns a literal NaN.
   */
  private def getReferenceExpr(name: String, references: Map[String, Signal[Expr]]): Expr = {
    references.get(name).fold[Expr] {
      Literal(Double.NaN)
    } { exprSignal =>
      exprSignal()
    }
  }
}

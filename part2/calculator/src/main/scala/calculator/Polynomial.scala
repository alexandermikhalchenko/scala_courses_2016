package calculator

object Polynomial {
  def computeDelta(a: Signal[Double], b: Signal[Double],
      c: Signal[Double]): Signal[Double] = {
    Signal(b() * b() - 4 * a() * c())
  }

  def computeSolutions(a: Signal[Double], b: Signal[Double],
      c: Signal[Double], delta: Signal[Double]): Signal[Set[Double]] = {
    Signal {
      if (delta() < 0) Set.empty
      else if (delta() == 0) Set(-b() / (2 * a()))
      else {
        val deltaSqrt = math.sqrt(delta())
        val a2 = 2 * a()
        Set (
          (-b() + deltaSqrt) / a2,
          (-b() - deltaSqrt) / a2
        )
      }
    }
  }
}

package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  lazy val genHeap: Gen[H] = for {
    h <- arbitrary[A]
    heap <- oneOf(const(empty), genHeap)
  } yield insert(h, heap)
  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

  property("gen1") = forAll { (h1: H, h2: H, h3: H) =>
    toSeq(meld(meld(h1, h2), h3)) == toSeq(meld(meld(h3, h1), h2))
  }

  private def toSeq(h: H): Seq[A] = {
    def hlp(h: H, s: Seq[A] = Seq.empty): Seq[A] = {
      if (isEmpty(h)) s
      else hlp(deleteMin(h), s :+ findMin(h))
    }

    hlp(h)
  }

}

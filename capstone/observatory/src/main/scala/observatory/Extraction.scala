package observatory

import java.time.LocalDate

import scala.io.Source

/**
  * 1st milestone: data extraction
  */
object Extraction {

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(year: Int, stationsFile: String, temperaturesFile: String): Iterable[(LocalDate, Location, Double)] = {
    val stations = readCsv(stationsFile)
      .flatMap(line => Station(line))
      .map(station => (station.stn, station.wban) -> station)
      .toMap

    readCsv(temperaturesFile)
      .flatMap { csvLine =>
        val date = LocalDate.of(year, csvLine(2).toInt, csvLine(3).toInt)
        stations.get(csvLine(0) -> csvLine(1))
          .map(_.location)
          .map { location =>
            val temp = fahrenheitToCelsius(csvLine(4).toDouble)
            (date, location, temp)
          }
      }
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(records: Iterable[(LocalDate, Location, Double)]): Iterable[(Location, Double)] = {
    records.toList.groupBy(_._2).map(kv => kv._1 -> kv._2.map(_._3).sum / kv._2.size)
  }

  private def fahrenheitToCelsius(fahrenheitDeg: Double): Double = (fahrenheitDeg - 32) / 1.8

  private def readCsv(file: String): Seq[Array[String]] = {
    val fileSource = Source.fromInputStream(getClass.getResourceAsStream(file))
    val csvLines = for (line <- fileSource.getLines())
      yield line.split(",", -1).map(_.trim)
    csvLines.toSeq
  }

  private case class Station(stn: String, wban: String, lat: Double, lon: Double) {
    lazy val location = Location(lat, lon)
  }

  private object Station {
    def apply(csvLine: Array[String]): Option[Station] = {
      if (csvLine(2).isEmpty || csvLine(3).isEmpty) None
      else Some(Station(csvLine(0), csvLine(1), csvLine(2).toDouble, csvLine(3).toDouble))
    }
  }
}

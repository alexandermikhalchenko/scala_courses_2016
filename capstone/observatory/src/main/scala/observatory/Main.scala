package observatory

import java.io.File

import com.sksamuel.scrimage.Image

object Main extends App {

//  InteractionRenderer.render()
  Visualization2Renderer.render()

}

object InteractionRenderer {

  def render(): Unit = {
    for {
      year <- 1975 to 2015
    } {
      if (!yearRendered(year)) {
        println(s"Extracting year $year")
        val temps = Extraction.locateTemperatures(year, "/stations.csv", s"/$year.csv")

        println(s"Averaging $year")
        val avgTemps = Extraction.locationYearlyAverageRecords(temps)

        println(s"Interaction $year")
        Interaction.generateTiles(Seq((year, avgTemps)), Interaction.generateImage)
      }
      println(s"========= $year completed =========")
    }
  }

  private def yearRendered(year: Int): Boolean = {
    val IMAGE_STORE_DIR = "target"
    val inputs = for {
      zoom <- 0 to 3
      subTileCoord <- Subtiles.subTileCoords(0, 0, zoom)
    } yield (zoom, subTileCoord.x, subTileCoord.y)

    inputs.forall { case (zoom, x, y) =>
      val dirPath = s"$IMAGE_STORE_DIR/temperatures/$year/$zoom"
      val file = new File(s"$dirPath/$x-$y.png")
      file.exists()
    }
  }
}

object Visualization2Renderer {

  private val IMAGE_STORE_DIR = "target"

  def render(): Unit = {
    val normals = Manipulation.average(averages)
    println("Calculating deviations")
    deviations(normals).foreach { yearlyData =>
      val year = yearlyData._1
      val grid = yearlyData._2
      val inputs = for {
        zoom <- 0 to 3
        subTileCoord <- Subtiles.subTileCoords(0, 0, zoom)
      } yield (zoom, subTileCoord.x, subTileCoord.y)

      inputs.par.foreach { case (zoom, x, y) =>
        val image = () => Visualization2.visualizeGrid(grid, TemperaturesColorMap.DEVIATIONS_COLORS, zoom, x, y)
        generateImage(year, zoom, x, y, image)
      }
    }

  }

  private def deviations(normals: (Int, Int) => Double): Seq[(Int, (Int, Int) => Double)] = {
    (1990 to 2015).filter(!yearRendered(_)).map { year =>
      println(s"Extracting year $year")
      val temps = Extraction.locateTemperatures(year, "/stations.csv", s"/$year.csv")
      println(s"Averaging $year")
      val averages = Extraction.locationYearlyAverageRecords(temps)
      (year, Manipulation.deviation(averages, normals))
    }.toList.sortBy(_._1)
  }

  private def averages = {
    (1975 to 1989).map { year =>
      println(s"Extracting year $year")
      val temps = Extraction.locateTemperatures(year, "/stations.csv", s"/$year.csv")
      println(s"Averaging $year")
      (year, Extraction.locationYearlyAverageRecords(temps))
    }.toList.sortBy(_._1).map(_._2)
  }

  private def generateImage(year: Int, zoom: Int, x: Int, y: Int, img: () => Image): Unit = Profiler.profile(s"generateImage $year $zoom $x $y") {
    val dirPath = s"$IMAGE_STORE_DIR/deviations/$year/$zoom"
    val file = new File(s"$dirPath/$x-$y.png")
    if (!file.exists()) {
      val dir = new File(dirPath)
      dir.mkdirs()
      img().output(file)
    }
  }

  private def yearRendered(year: Int): Boolean = {
    val inputs = for {
      zoom <- 0 to 3
      subTileCoord <- Subtiles.subTileCoords(0, 0, zoom)
    } yield (zoom, subTileCoord.x, subTileCoord.y)

    inputs.forall { case (zoom, x, y) =>
      val dirPath = s"$IMAGE_STORE_DIR/deviations/$year/$zoom"
      val file = new File(s"$dirPath/$x-$y.png")
      file.exists()
    }
  }
}

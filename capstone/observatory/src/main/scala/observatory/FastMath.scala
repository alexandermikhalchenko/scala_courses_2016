package observatory

trait Math {
  val PI: Double = java.lang.Math.PI

  def sin(a: Double): Double
  def cos(a: Double): Double
  def acos(a: Double): Double
  def atan(a: Double): Double
  def sinh(a: Double): Double
  def pow(a: Double, b: Double): Double
  def toDegrees(a: Double): Double
  def toRadians(a: Double): Double
  def abs(a: Double): Double
  def sqrt(a: Double): Double
}

trait RegularMath extends Math {
  def sin(a: Double): Double = java.lang.Math.sin(a)
  def cos(a: Double): Double = java.lang.Math.cos(a)
  def acos(a: Double): Double = java.lang.Math.acos(a)
  def atan(a: Double): Double = java.lang.Math.atan(a)
  def sinh(a: Double): Double = java.lang.Math.sinh(a)
  def pow(a: Double, b: Double): Double = java.lang.Math.pow(a, b)
  def toDegrees(angrad: Double): Double = java.lang.Math.toDegrees(angrad)
  def toRadians(angdeg: Double): Double = java.lang.Math.toRadians(angdeg)
  def abs(a: Double): Double = java.lang.Math.abs(a)
  def sqrt(a: Double): Double = java.lang.Math.sqrt(a)
}

//trait FastMath extends Math {
//  def sin(a: Double): Double = net.jafama.FastMath.sin(a)
//  def cos(a: Double): Double = net.jafama.FastMath.cos(a)
//  def acos(a: Double): Double = net.jafama.FastMath.acos(a)
//  def atan(a: Double): Double = net.jafama.FastMath.atan(a)
//  def sinh(a: Double): Double = net.jafama.FastMath.sinh(a)
//  def pow(a: Double, b: Double): Double = net.jafama.FastMath.pow(a, b)
//  def toDegrees(angrad: Double): Double = net.jafama.FastMath.toDegrees(angrad)
//  def toRadians(angdeg: Double): Double = net.jafama.FastMath.toRadians(angdeg)
//  def abs(a: Double): Double = net.jafama.FastMath.abs(a)
//  def sqrt(a: Double): Double = net.jafama.FastMath.sqrt(a)
//}

object Math extends Math with RegularMath

package observatory

import java.util.concurrent.ConcurrentHashMap

/**
  * 4th milestone: value-added information
  */
object Manipulation {

  /**
    * @param temperatures Known temperatures
    * @return A function that, given a latitude in [-89, 90] and a longitude in [-180, 179],
    *         returns the predicted temperature at this location
    */
  def makeGrid(temperatures: Iterable[(Location, Double)]): (Int, Int) => Double = {
    (lat, lon) => Visualization.predictTemperature(temperatures, Location(lat, lon))
  }

  /**
    * @param temperaturess Sequence of known temperatures over the years (each element of the collection
    *                      is a collection of pairs of location and temperature)
    * @return A function that, given a latitude and a longitude, returns the average temperature at this location
    */
  def average(temperaturess: Iterable[Iterable[(Location, Double)]]): (Int, Int) => Double = {
    val grids = temperaturess.map(makeGrid)
    val cache = new ConcurrentHashMap[(Int, Int), java.lang.Double]() {
      override def get(key: scala.Any): java.lang.Double = {
        val existing = Option(super.get(key))
        existing.getOrElse {
          val (lat, lon) = key.asInstanceOf[(Int, Int)]
          val newVal = {
            val (count, sum) = grids.foldLeft(0, 0.0)((s, grid) => {
              s._1 + 1 -> (s._2 + grid(lat, lon))
            })
            sum / count
          }
          put((lat, lon), newVal)
          newVal
        }
      }
    }

    (lat, lon) => {
      cache.get((lat, lon))
    }
  }

  /**
    * @param temperatures Known temperatures
    * @param normals A grid containing the “normal” temperatures
    * @return A sequence of grids containing the deviations compared to the normal temperatures
    */
  def deviation(temperatures: Iterable[(Location, Double)], normals: (Int, Int) => Double): (Int, Int) => Double = {
    val cache = new ConcurrentHashMap[(Int, Int), java.lang.Double]() {
      override def get(key: scala.Any): java.lang.Double = {
        val existing = Option(super.get(key))
        existing.getOrElse {
          val (lat, lon) = key.asInstanceOf[(Int, Int)]
          val newVal = Visualization.predictTemperature(temperatures, Location(lat, lon)) - normals(lat, lon)
          newVal
        }
      }
    }

    (lat, lon) => cache.get((lat, lon))
  }


}


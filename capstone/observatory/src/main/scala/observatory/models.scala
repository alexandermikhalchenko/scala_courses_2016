package observatory

case class Location(lat: Double, lon: Double)

case class Color(red: Int, green: Int, blue: Int)

object Subtiles {
  def apply(x: Int, y: Int): Subtiles = new Subtiles(x, y)

  def subTileCoords(x: Int, y: Int, maxZoom: Int): Seq[Subtile] = {
    if (maxZoom == 0) Seq(Subtile(x, y))
    else Subtiles(x, y).toSeq.flatMap(st => subTileCoords(st.x, st.y, maxZoom - 1))
  }

}
class Subtiles(x: Int, y: Int) {
  val tl = Subtile(2 * x,       2 * y)
  val tr = Subtile(2 * x + 1,   2 * y)
  val bl = Subtile(2 * x,       2 * y + 1)
  val br = Subtile(2 * x + 1,   2 * y + 1)
  lazy val toSeq = Seq(tl, tr, bl, br)
}
case class Subtile(x: Int, y: Int)

object TemperaturesColorMap {

  val TEMPERATURES_COLORS = List(
    (60.0, Color(255, 255, 255)),
    (32.0, Color(255, 0, 0)),
    (12.0, Color(255, 255, 0)),
    (0.0, Color(0, 255, 255)),
    (-15.0, Color(0, 0, 255)),
    (-27.0, Color(255, 0, 255)),
    (-50.0, Color(33, 0, 107)),
    (-60.0, Color(0, 0, 0))
  )

  val DEVIATIONS_COLORS = List(
    (7.0, Color(0, 0, 0)),
    (4.0, Color(255, 0, 0)),
    (2.0, Color(255, 255, 0)),
    (0.0, Color(255, 255, 255)),
    (-2.0, Color(0, 255, 255)),
    (-7.0, Color(0, 0, 255))
  )

}
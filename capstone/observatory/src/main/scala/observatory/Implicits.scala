package observatory

import com.sksamuel.scrimage.Pixel

object Implicits {

  implicit class RichColor(color: Color) {
    def pixel(alpha: Int): Pixel = {
      Pixel(color.red, color.green, color.blue, alpha)
    }
  }

}

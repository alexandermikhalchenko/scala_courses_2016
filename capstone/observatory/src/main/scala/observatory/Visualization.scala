package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import Math._

/**
  * 2nd milestone: basic visualization
  */
object Visualization {

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Double)], location: Location): Double = {
    val sums =
      temperatures.foldLeft((Double.NaN, (0.0, 0.0))) { (acc, lt) =>
        if (!acc._1.isNaN) { acc }
        else {
          val gcd = greatCircleDistance(lt._1, location)
          if (gcd < 1) { (lt._2, acc._2) }
          else {
            val wVal = w(gcd)
            (acc._1, (acc._2._1 + wVal * lt._2, acc._2._2 + wVal))
          }
        }
      }

    if (sums._1.isNaN) {
      sums._2._1 / sums._2._2
    } else {
      sums._1
    }
  }

  private val p = 3.0

  private def w(gcd: Double): Double = {
    1 / pow(gcd, p)
  }

  private val r = 6371

  def greatCircleDistance(l1: Location, l2: Location): Double = {
    val dLambda = abs(toRadians(l2.lon) - toRadians(l1.lon))
    val l1LatRad = toRadians(l1.lat)
    val l2LatRad = toRadians(l2.lat)
    val a = sin(l1LatRad) * sin(l2LatRad) + cos(l1LatRad) * cos(l2LatRad) * cos(dLambda)
    val fixedA = if (a > 1) 1 else if (a < -1) -1 else a
    val dSigma = acos(fixedA)
    r * dSigma
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Double, Color)], value: Double): Color = {
    val pointsSorted = points.toList.sortBy(_._1)
    (pointsSorted zip pointsSorted.tail)
      .find(range => range._1._1 <= value && range._2._1 >= value)
      .map { range =>
        val t = (value - range._1._1) / (range._2._1 - range._1._1)
        lerpRGB(range._1._2, range._2._2, t)
      }
      .getOrElse {
        if (value <= pointsSorted.head._1) pointsSorted.head._2
        else pointsSorted.last._2
      }
  }

  private def lerpRGB(a: Color, b: Color, t: Double) = {
    def lerp(c1: Int, c2: Int): Int =
      (c1 * (1 - t) + c2 * t + 0.5).toInt

    Color(
      lerp(a.red, b.red),
      lerp(a.green, b.green),
      lerp(a.blue, b.blue)
    )
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)]): Image = {
    val img = Image(360, 180)
    val coords =
      for {
        x <- 0 until 360
        y <- 0 until 180
      } yield (x, y)

    coords.par.foreach { case (x, y) =>
        val gpsX = x - 180
        val gpsY = -y + 90
        val temperature = predictTemperature(temperatures, Location(gpsY, gpsX))
        val color = interpolateColor(colors, temperature)
        val pixel = Pixel(color.red, color.green, color.blue, 255)
        img.setPixel(x, y, pixel)
    }
    img
  }

}


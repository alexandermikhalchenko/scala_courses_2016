package observatory

import com.sksamuel.scrimage.Image
import observatory.Interaction.tileLocation
import observatory.Visualization.interpolateColor
import scala.language.implicitConversions

/**
  * 5th milestone: value-added information visualization
  */
object Visualization2 {

  /**
    * @param x X coordinate between 0 and 1
    * @param y Y coordinate between 0 and 1
    * @param d00 Top-left value
    * @param d01 Bottom-left value
    * @param d10 Top-right value
    * @param d11 Bottom-right value
    * @return A guess of the value at (x, y) based on the four known values, using bilinear interpolation
    *         See https://en.wikipedia.org/wiki/Bilinear_interpolation#Unit_Square
    */
  def bilinearInterpolation(
    x: Double,
    y: Double,
    d00: Double,
    d01: Double,
    d10: Double,
    d11: Double
  ): Double = {
    d00 * (1.0 - x) * (1.0 - y) + d10 * x * (1.0 - y) + d01 * (1.0 - x) * y + d11 * x * y
  }

  /**
    * @param grid Grid to visualize
    * @param colors Color scale to use
    * @param zoom Zoom level of the tile to visualize
    * @param x X value of the tile to visualize
    * @param y Y value of the tile to visualize
    * @return The image of the tile at (x, y, zoom) showing the grid using the given color scale
    */
  def visualizeGrid(
    grid: (Int, Int) => Double,
    colors: Iterable[(Double, Color)],
    zoom: Int,
    x: Int,
    y: Int
  ): Image = {
    subTiles(zoom, 256, x, y)(grid, colors)
  }

  private def subTiles(zoom0: Int, imgSize0: Int, x0: Int, y0: Int)
                      (implicit grid: (Int, Int) => Double, colors: Iterable[(Double, Color)]): Image = {

    val xShift = x0 * imgSize0
    val yShift = y0 * imgSize0
    val img = Image(imgSize0, imgSize0)

    def loop(zoom: Int, imgSize: Int, x: Int, y: Int): Unit = {
      import Implicits.RichColor
      if (imgSize == 1) {
        val color = colorAt(zoom, x, y, grid, colors)
        val imgX = x - xShift
        val imgY = y - yShift
        img.setPixel(imgX, imgY, color.pixel(127))
      } else {
        val nextZoom = zoom + 1
        val nextImageSize = imgSize / 2
        val subtiles = Subtiles(x, y)

        loop(nextZoom, nextImageSize, subtiles.tl.x, subtiles.tl.y)
        loop(nextZoom, nextImageSize, subtiles.tr.x, subtiles.tr.y)
        loop(nextZoom, nextImageSize, subtiles.bl.x, subtiles.bl.y)
        loop(nextZoom, nextImageSize, subtiles.br.x, subtiles.br.y)
      }
    }

    loop(zoom0, imgSize0, x0, y0)
    img
  }

  private def colorAt(zoom: Int, x: Int, y: Int, grid: (Int, Int) => Double, colors: Iterable[(Double, Color)]): Color = {
    val location = tileLocation(zoom, x, y)

    val lon = location.lon
    val lat = location.lat

    val minX = lat.floor.toInt
    val maxX = lat.ceil.toInt

    val minY = lon.floor.toInt
    val maxY = lon.ceil.toInt

    val bilinearX = if (minX == maxX) minX else (lat - minX) / (maxX - minX)
    val bilinearY = if (minY == maxY) minY else (lon - minY) / (maxY - minY)

    val d00 = grid(minX, minY)
    val d01 = grid(minX, maxY)
    val d10 = grid(maxX, minY)
    val d11 = grid(maxX, maxY)

    val temp = bilinearInterpolation(bilinearX, bilinearY, d00, d01, d10, d11)

    interpolateColor(colors, temp)
  }

}

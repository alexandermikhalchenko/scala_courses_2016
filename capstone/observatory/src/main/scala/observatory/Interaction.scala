package observatory

import java.io.File

import com.sksamuel.scrimage.Image
import observatory.Math._
import observatory.Visualization.{interpolateColor, predictTemperature}
import scala.language.implicitConversions

/**
  * 3rd milestone: interactive visualization
  */
object Interaction {

//  val IMAGE_STORE_DIR = "target"
  val IMAGE_STORE_DIR = "/Users/aam/ownCloud"

  /**
    * @param zoom Zoom level
    * @param x X coordinate
    * @param y Y coordinate
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(zoom: Int, x: Int, y: Int): Location = {
    val n = pow(2, zoom)
    val lonDeg = x / n * 360.0 - 180.0
    val latRad = atan(sinh(PI * (1 - 2 * y / n)))
    val latDeg = toDegrees(latRad)
    Location(latDeg, lonDeg)
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @param zoom Zoom level
    * @param x X coordinate
    * @param y Y coordinate
    * @return A 256×256 image showing the contents of the tile defined by `x`, `y` and `zooms`
    */
  def tile(temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)], zoom: Int, x: Int, y: Int): Image = {
    val imageSize = 256
    subTiles(zoom, imageSize, x, y)(temperatures, colors).scale(256 / imageSize)
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    * @param yearlyData Sequence of (year, data), where `data` is some data associated with
    *                   `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](
    yearlyData: Iterable[(Int, Data)],
    generateImage: (Int, Int, Int, Int, Data) => Unit
  ): Unit = {
    val inputs = for {
      (year, data) <- yearlyData
      zoom <- 0 to 3
      subTileCoord <- Subtiles.subTileCoords(0, 0, zoom)
    } yield (year, zoom, subTileCoord.x, subTileCoord.y, data)

    inputs.par.foreach(in => {
      generateImage.tupled(in)
    })
  }

  private def colorAt(zoom: Int, x: Int, y: Int, temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)]): Color = {
    val location = tileLocation(zoom, x, y)
    val temp = predictTemperature(temperatures, location)
    interpolateColor(colors, temp)
  }

  private def subTiles(zoom0: Int, imgSize0: Int, x0: Int, y0: Int)
                     (implicit temperatures: Iterable[(Location, Double)], colors: Iterable[(Double, Color)]): Image = {

    val xShift = x0 * imgSize0
    val yShift = y0 * imgSize0
    val img = Image(imgSize0, imgSize0)

    def loop(zoom: Int, imgSize: Int, x: Int, y: Int): Unit = {
      import Implicits.RichColor
      if (imgSize == 1) {
        val color = colorAt(zoom, x, y, temperatures, colors)
        val imgX = x - xShift
        val imgY = y - yShift
        img.setPixel(imgX, imgY, color.pixel(127))
      } else {
        val nextZoom = zoom + 1
        val nextImageSize = imgSize / 2
        val subtiles = Subtiles(x, y)

        loop(nextZoom, nextImageSize, subtiles.tl.x, subtiles.tl.y)
        loop(nextZoom, nextImageSize, subtiles.tr.x, subtiles.tr.y)
        loop(nextZoom, nextImageSize, subtiles.bl.x, subtiles.bl.y)
        loop(nextZoom, nextImageSize, subtiles.br.x, subtiles.br.y)
      }
    }

    loop(zoom0, imgSize0, x0, y0)
    img
  }

  def generateImage(year: Int, zoom: Int, x: Int, y: Int, data: Iterable[(Location, Double)]): Unit = Profiler.profile(s"generateImage $year $zoom $x $y") {
    val dirPath = s"$IMAGE_STORE_DIR/temperatures/$year/$zoom"
    val file = new File(s"$dirPath/$x-$y.png")
    if (!file.exists()) {
      val dir = new File(dirPath)
      dir.mkdirs()
      val img = tile(data, TemperaturesColorMap.TEMPERATURES_COLORS, zoom, x, y)
      img.output(file)
    }
  }

}

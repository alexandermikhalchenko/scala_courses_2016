package observatory

/**
  * Created by Alexander on 23.04.2017.
  */
object Profiler {

  def profile[T](name: String = "")(func: => T): T = {
    val start = System.currentTimeMillis()
    val res = func
    println(s"$name finished in ${System.currentTimeMillis() - start}ms")
    res
  }

}

package observatory

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers

import scala.collection.concurrent.TrieMap

@RunWith(classOf[JUnitRunner])
class InteractionTest extends FunSuite with Checkers {

  test("draw test image") {
    val temps = Extraction.locateTemperatures(2021, "/interaction/stations.csvtest", "/interaction/2021.csvtest")
    val avgTemps = Extraction.locationYearlyAverageRecords(temps)
    val img = Interaction.tile(avgTemps,TemperaturesColorMap.TEMPERATURES_COLORS, 0, 0, 0)
    img.output("G:\\Documents\\Idea_projects\\scala_courses_2016\\capstone\\observatory\\target\\drawTestImage.png")
  }

  test("draw test image 2") {
    val temps = Extraction.locateTemperatures(2022, "/interaction/stations2.csvtest", "/interaction/2022.csvtest")
    val avgTemps = Extraction.locationYearlyAverageRecords(temps)
    val img = Interaction.tile(avgTemps, TemperaturesColorMap.TEMPERATURES_COLORS, 0, 0, 0)
    img.output("G:\\Documents\\Idea_projects\\scala_courses_2016\\capstone\\observatory\\target\\drawTestImage2.png")
  }

}

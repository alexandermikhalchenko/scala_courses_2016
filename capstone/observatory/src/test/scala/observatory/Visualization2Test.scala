package observatory

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers

@RunWith(classOf[JUnitRunner])
class Visualization2Test extends FunSuite with Checkers {

  test("bilinear interpolation") {
    val actual = Visualization2.bilinearInterpolation(0.5, 0.5, 3, 5, 7, 11)
    assert(6.5 === actual)
  }

  test("visualize grid") {
    val grid = (lat: Int, lon: Int) => if (lat == 0 && (lon == 0 || lon == 2)) -7.0 else 4.0
    val image = Visualization2.visualizeGrid(grid, TemperaturesColorMap.DEVIATIONS_COLORS, 0, 0, 0)
    image.output("G:\\Documents\\Idea_projects\\scala_courses_2016\\capstone\\observatory\\target\\grid.png")
  }

}

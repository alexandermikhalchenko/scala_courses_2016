package observatory


import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.prop.Checkers

@RunWith(classOf[JUnitRunner])
class VisualizationTest extends FunSuite with Checkers {

  import Visualization._

  test("predict temperature") {
    val temp1 = 100.0
    val temp2 = -100.0
    val result = predictTemperature(
      List(
        (Location(-180.0, 90.0), temp1),
        (Location(179.0, 90.0), temp2)
      ),
      Location(0.0, 90.0)
    )

    assert(Math.abs(temp2 - result) < Math.abs(temp1 - result))
  }

  test("predict temperature2") {
    val temp1 = 100.0
    val temp2 = -100.0
    val result = predictTemperature(
      List(
        (Location(-180.0, 90.0), temp1),
        (Location(179.0, 90.0), temp2)
      ),
      Location(-1.0, 90.0)
    )

    assert(Math.abs(temp2 - result) > Math.abs(temp1 - result))
  }

  test("predictTemperature: some point closer") {
    val location1 = Location(1,1)
    val temp1 = 10d
    val location2 = Location(-10,-10)
    val temp2 = 50d
    val list = List(
      (location1, temp1),
      (location2, temp2)
    )
    val result = predictTemperature(list, Location(0, 0))
    assert(temp1 - result < temp2 - result)
  }

  test("interpolate color 1") {
    val color1 = Color(255, 0, 0)
    val color2 = Color(0, 0, 255)
    val interpolated = interpolateColor(
      List(
        (0.0, color1),
        (100.0, color2)
      ),
      0
    )

    assert(
      colorDistance(color1, interpolated) < colorDistance(color2, interpolated)
    )
  }

  test("interpolate color 2") {
    val color1 = Color(255, 0, 0)
    val color2 = Color(0, 0, 255)
    val interpolated = interpolateColor(
      List(
        (0.0, color1),
        (100.0, color2)
      ),
      100
    )

    assert(
      colorDistance(color1, interpolated) > colorDistance(color2, interpolated)
    )
  }

  test("interpolate color 3") {
    val color1 = Color(255, 0, 0)
    val color2 = Color(0, 0, 255)
    val interpolated = interpolateColor(
      List(
        (0.0, color1),
        (100.0, color2)
      ),
      49.9
    )

    assert(
      colorDistance(color1, interpolated) < colorDistance(color2, interpolated)
    )
  }

  test("interpolate color 4") {
    val color1 = Color(255, 0, 0)
    val color2 = Color(0, 0, 255)
    val interpolated = interpolateColor(
      List(
        (0.0, color1),
        (100.0, color2)
      ),
      50.1
    )

    assert(
      colorDistance(color1, interpolated) > colorDistance(color2, interpolated)
    )
  }

  private def colorDistance(c1: Color, c2: Color): Double = {
    Math.sqrt(
      (c2.red - c1.red) * (c2.red - c1.red) +
      (c2.green - c1.green) * (c2.green - c1.green) +
      (c2.blue - c1.blue) * (c2.blue - c1.blue)
    )
  }

  test("predictTemperature small sets") {
    assert(Visualization.predictTemperature(List((Location(45.0, -90.0), 10.0), (Location(-45.0, 0.0), 20.0)), Location(0.0, -45.0)) === 15.0)
    assert(Visualization.predictTemperature(List((Location(0.0, 0.0), 10.0)), Location(0.0, 0.0)) === 10.0)
    assert(Visualization.predictTemperature(List((Location(-180, 90.0), 0.0), (Location(179, -89), 100)), Location(-0.5, 0.5)).round === 50)
    assert(Visualization.predictTemperature(List((Location(45.0, -90.0), 0.0), (Location(-45.0, 0.0), 59.028308521858634)), Location(0.0, 0.0)).round === 52)
  }



  test("interpolateColor") {
    val palette = List(
      (100.0, Color(255, 255, 255)),
      (50.0, Color(0, 0, 0)),
      (0.0, Color(255, 0, 128))
    )

    assert(Visualization.interpolateColor(palette, 50.0) === Color(0, 0, 0))
    assert(Visualization.interpolateColor(palette, 0.0) === Color(255, 0, 128))
    assert(Visualization.interpolateColor(palette, -10.0) === Color(255, 0, 128))
    assert(Visualization.interpolateColor(palette, 200.0) === Color(255, 255, 255))
    assert(Visualization.interpolateColor(palette, 75.0) === Color(128, 128, 128))
    assert(Visualization.interpolateColor(palette, 25.0) === Color(128, 0, 64))
  }

  test("crsr") {
    val temperatures = List((Location(45.0,-90.0),1.0), (Location(-45.0,0.0),-67.45977286686251))
    val colors = List((1.0,Color(255,0,0)), (-67.45977286686251,Color(0,0,255)))

    val gpsX = -180
    val gpsY = -27
    val x = gpsX + 180
    val y = -gpsY + 90

    val pixel = visualize(temperatures, colors).pixel((x, y))
    val resColor = Color(pixel.red, pixel.green, pixel.blue)

    assert(
      colorDistance(resColor, Color(0,0,255)) < colorDistance(resColor, Color(255,0,0))
    )
  }

  test("Color interpolation 1") {
    val scale = List((0.0, Color(0, 0, 255)))
    assert(interpolateColor(scale, -0.5) == Color(0, 0, 255))
    assert(interpolateColor(scale, 0.5) == Color(0, 0, 255))
    assert(interpolateColor(scale, 0.0) == Color(0, 0, 255))
  }

  test("Color interpolation 2") {
    val colors =
      List((60.0, Color(255,255,255)), (32.0, Color(255, 0, 0)),
        (12.0, Color(255, 255, 0)), (0.0, Color(0, 255, 255)),
        (-15.0, Color(0, 0, 255)), (-27.0, Color(255, 0, 255)),
        (-50.0, Color(33, 0, 107)), (-60.0, Color(0, 0, 0)))
    assert(interpolateColor(colors, 12.0) == Color(255, 255, 0))
    assert(interpolateColor(colors, 62) == Color(255, 255, 255))
    assert(interpolateColor(colors, 6) == Color(128, 255, 128))
  }

  test("visualize2") {
    Visualization.visualize(
      List((Location(45.0,-90.0),36.1643835403579), (Location(-45.0,0.0),-21.219052136494582)),
      List((36.1643835403579, Color(255,0,0)), (-21.219052136494582, Color(0,0,255)))
    )
  }

}

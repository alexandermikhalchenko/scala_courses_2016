package observatory

import java.time.LocalDate

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ExtractionTest extends FunSuite {

  test("parse simple csv files") {
    assert(Extraction.locateTemperatures(2015, "/extraction/stations.csvtest", "/extraction/temperatures.csvtest").toSeq ===
      Stream(
        (LocalDate.of(2015, 8, 11), Location(37.350, -78.433), 27.3),
        (LocalDate.of(2015, 12, 6), Location(37.358, -78.438), 0),
        (LocalDate.of(2015, 1, 29), Location(37.358, -78.438), 2.000000000000001)
      ))
  }

  test("compute the average temperature, over a year, for every station") {
    assert(Extraction.locationYearlyAverageRecords(Seq(
      (LocalDate.of(2015, 8, 11), Location(37.350, -78.433), 81.14),
      (LocalDate.of(2015, 12, 6), Location(37.358, -78.438), 32.0),
      (LocalDate.of(2015, 1, 29), Location(37.358, -78.438), 35.6)
    )).toList ===
      List(
        (Location(37.358, -78.438), 33.8),
        (Location(37.350, -78.433), 81.14)
      ))
  }

}
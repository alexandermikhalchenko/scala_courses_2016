package barneshut

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SimulatorSuite extends FunSuite {

  test("updateBoundaries should be correctly updated if point is inside it") {
    val bSrc = Boundaries(0, 0, 10, 10)
    val bExpected = Boundaries(0, 0, 10, 10)
    val body = new Body(0, 5, 5, 0, 0)
    val model = new SimulationModel
    val simulator = new Simulator(model.taskSupport, model.timeStats)
    assert(simulator.updateBoundaries(bSrc, body) == bExpected)
  }

  test("updateBoundaries should be correctly updated if point is outside it") {
    val bSrc = Boundaries(0, 0, 10, 10)
    val bExpected = Boundaries(0, 0, 15, 15)
    val body = new Body(0, 15, 15, 0, 0)
    val model = new SimulationModel
    val simulator = new Simulator(model.taskSupport, model.timeStats)
    assert(simulator.updateBoundaries(bSrc, body) == bExpected)
  }

  test("mergeBoundaries should correctly merge boundaries") {
    val b1 = Boundaries(0, 0, 10, 10)
    val b2 = Boundaries(5, 5, 15, 15)
    val bExpected = Boundaries(0, 0, 15, 15)
    val model = new SimulationModel
    val simulator = new Simulator(model.taskSupport, model.timeStats)
    assert(simulator.mergeBoundaries(b1, b2) == bExpected)
  }

}

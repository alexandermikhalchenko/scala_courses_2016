
import common._

package object scalashop {

  /** The value of every pixel is represented as a 32 bit integer. */
  type RGBA = Int

  /** Returns the red component. */
  def red(c: RGBA): Int = (0xff000000 & c) >>> 24

  /** Returns the green component. */
  def green(c: RGBA): Int = (0x00ff0000 & c) >>> 16

  /** Returns the blue component. */
  def blue(c: RGBA): Int = (0x0000ff00 & c) >>> 8

  /** Returns the alpha component. */
  def alpha(c: RGBA): Int = (0x000000ff & c) >>> 0

  /** Used to create an RGBA value from separate components. */
  def rgba(r: Int, g: Int, b: Int, a: Int): RGBA = {
    (r << 24) | (g << 16) | (b << 8) | (a << 0)
  }

  /** Restricts the integer into the specified range. */
  def clamp(v: Int, min: Int, max: Int): Int = {
    if (v < min) min
    else if (v > max) max
    else v
  }

  /** Image is a two-dimensional matrix of pixel values. */
  class Img(val width: Int, val height: Int, private val data: Array[RGBA]) {
    def this(w: Int, h: Int) = this(w, h, new Array(w * h))
    def apply(x: Int, y: Int): RGBA = data(y * width + x)
    def update(x: Int, y: Int, c: RGBA): Unit = data(y * width + x) = c
  }

  def split(c: RGBA): Seq[Int] = Seq(red(c), green(c), blue(c), alpha(c))
  def sum(acc: Seq[Int], c: RGBA): Seq[Int] = {
    acc.zip(split(c)).map(t => t._1 + t._2)
  }
  def avg(c: Seq[Int], d: Int): Int = rgba(c(0) / d, c(1) / d, c(2) / d, c(3) / d)

  /** Computes the blurred RGBA value of a single pixel of the input image. */
  def boxBlurKernel(src: Img, x: Int, y: Int, radius: Int): RGBA = {
    if (radius == 0) src(x, y)
    else {
      val pixels = for {
        curX <- clamp(x - radius, 0, src.width - 1) to clamp(x + radius, 0, src.width - 1)
        curY <- clamp(y - radius, 0, src.height - 1) to clamp(y + radius, 0, src.height - 1)
      } yield src(curX, curY)

      val pxSum = pixels.foldLeft(Seq(0, 0, 0, 0))(sum)
      avg(pxSum, pixels.size)
    }
  }

}

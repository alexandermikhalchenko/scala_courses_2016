package scalashop

import org.scalameter._
import common._

object VerticalBoxBlurRunner {

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 5,
    Key.exec.maxWarmupRuns -> 10,
    Key.exec.benchRuns -> 10,
    Key.verbose -> false
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val radius = 3
    val width = 1920
    val height = 1080
    val src = new Img(width, height)
    val dst = new Img(width, height)
    println("start measure")
    val seqtime = standardConfig measure {
      println("start sequential blur")
      VerticalBoxBlur.blur(src, dst, 0, width, radius)
      println("end sequential blur")
    }
    println(s"sequential blur time: $seqtime ms")

    val numTasks = 32
    println("start par measure")
    val partime = standardConfig measure {
      println("start par blur")
      VerticalBoxBlur.parBlur(src, dst, numTasks, radius)
      println("end par blur")
    }
    println(s"fork/join blur time: $partime ms")
    println(s"speedup: ${seqtime / partime}")
  }

}

/** A simple, trivially parallelizable computation. */
object VerticalBoxBlur {

  /** Blurs the columns of the source image `src` into the destination image
   *  `dst`, starting with `from` and ending with `end` (non-inclusive).
   *
   *  Within each column, `blur` traverses the pixels by going from top to
   *  bottom.
   */
  def blur(src: Img, dst: Img, from: Int, end: Int, radius: Int): Unit = {
    for {
      curX <- from until end
      curY <- 0 until src.height
    } yield dst(curX, curY) = boxBlurKernel(src, curX, curY, radius)
  }

  /** Blurs the columns of the source image in parallel using `numTasks` tasks.
   *
   *  Parallelization is done by stripping the source image `src` into
   *  `numTasks` separate strips, where each strip is composed of some number of
   *  columns.
   */
  def parBlur(src: Img, dst: Img, numTasks: Int, radius: Int): Unit = {
    val stripWidth = src.width / numTasks
    val tasks = for {
      strip <- 1 to numTasks
      from = stripWidth * (strip - 1)
      end = if (strip != numTasks) from + stripWidth else src.width
    } yield task(blur(src, dst, from, end, radius))

    tasks.foreach(_.join())
  }

}

package reductions

import scala.annotation._
import org.scalameter._
import common._

object ParallelParenthesesBalancingRunner {

  @volatile var seqResult = false

  @volatile var parResult = false

  val standardConfig = config(
    Key.exec.minWarmupRuns -> 40,
    Key.exec.maxWarmupRuns -> 80,
    Key.exec.benchRuns -> 120,
    Key.verbose -> true
  ) withWarmer(new Warmer.Default)

  def main(args: Array[String]): Unit = {
    val length = 100000000
    val chars = new Array[Char](length)
    val threshold = 10000
    val seqtime = standardConfig measure {
      seqResult = ParallelParenthesesBalancing.balance(chars)
    }
    println(s"sequential result = $seqResult")
    println(s"sequential balancing time: $seqtime ms")

    val fjtime = standardConfig measure {
      parResult = ParallelParenthesesBalancing.parBalance(chars, threshold)
    }
    println(s"parallel result = $parResult")
    println(s"parallel balancing time: $fjtime ms")
    println(s"speedup: ${seqtime / fjtime}")
  }
}

object ParallelParenthesesBalancing {

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def balance(chars: Array[Char]): Boolean = {
    var counter = 0
    var idx = 0
    while(idx < chars.length && counter >= 0) {
      if (chars(idx) == '(') counter += 1
      if (chars(idx) == ')') counter -= 1
      idx += 1
    }
    counter == 0
  }

  /** Returns `true` iff the parentheses in the input `chars` are balanced.
   */
  def parBalance(chars: Array[Char], threshold: Int): Boolean = {

    def traverse(idx: Int, until: Int, arg1: Int, arg2: Int): (Int, Int) = {
      var total = 0
      var min = 0
      var i = idx
      while (i != until) {
        chars(i) match {
          case '(' => total += 1
          case ')' => total -= 1; min = if (total < min) total else min
          case _ =>
        }
        i += 1
      }
      (total, min)
    }

    def reduce(from: Int, until: Int): (Int, Int) = {
      if (until - from > threshold) {
        val middle = (until + from) / 2
        val (l, r) = parallel(reduce(from, middle), reduce(middle, until))
        val total = l._1 + r._1
        val minLeft = Math.min(0, l._2)
        val minRight = Math.min(minLeft, l._1 + r._2)
        (total, minRight)
      } else traverse(from, until, 0, 0)
    }

    val res = reduce(0, chars.length)
    res == (0, 0)
  }

  // For those who want more:
  // Prove that your reduction operator is associative!

}

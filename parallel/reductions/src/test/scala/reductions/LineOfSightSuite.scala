package reductions

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import scala.util.Random

@RunWith(classOf[JUnitRunner]) 
class LineOfSightSuite extends FunSuite {
  import LineOfSight._
  test("lineOfSight should correctly handle an array of size 4") {
    val output = new Array[Float](4)
    lineOfSight(Array[Float](0f, 1f, 8f, 9f), output)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }

  test("parLineOfSight should correctly handle an array") {
    val size = 1000
    val seqOutput = new Array[Float](size)
    val parOutput = new Array[Float](size)
    val input = new Array[Float](size)
    var i = 0
    while (i < input.length) {
      input(i) = Random.nextFloat()
      i += 1
    }
    lineOfSight(input, seqOutput)
    parLineOfSight(input, parOutput, 100)
    assert(seqOutput.toList == parOutput.toList)
  }


  test("upsweepSequential should correctly handle the chunk 1 until 4 of an array of 4 elements") {
    val res = upsweepSequential(Array[Float](0f, 1f, 8f, 9f), 1, 4)
    assert(res == 4f)
  }

  test("upsweep should correctly handle the chunk 1 until 4 of an array of 4 elements") {
    val res = upsweep(Array[Float](0f, 1f, 8f, 9f), 1, 4, 5)
    assert(res == Leaf(1, 4, 4f))
  }

  test("upsweep should correctly handle the chunk 1 until 10 of an array of 10 elements") {
    val input = Array[Float](0f, 1f, 8f, 9f, 10f, 3f, 5f, 4f, 8f, 11f)
    val res = upsweep(input, 1, 10, 5)
    val left = upsweepSequential(input, 1, 5)
    val right = upsweepSequential(input, 5, 10)
    assert(res == Node(Leaf(1, 5, left), Leaf(5, 10, right)))
  }

  test("upsweep 2 should correctly handle the chunk 1 until 10 of an array of 10 elements") {
    val input = Array[Float](0.48561686f, 0.19934845f, 0.1373266f, 0.8692405f)
    val res = upsweep(input, 1, 4, 2)
    val left = upsweepSequential(input, 1, 2)
    val right = upsweepSequential(input, 2, 4)
    assert(res == Node(Leaf(1, 2, left), Leaf(2, 4, right)))
  }

  test("downsweepSequential should correctly handle a 4 element array when the starting angle is zero") {
    val output = new Array[Float](4)
    downsweepSequential(Array[Float](0f, 1f, 8f, 9f), output, 0f, 1, 4)
    assert(output.toList == List(0f, 1f, 4f, 4f))
  }

}

